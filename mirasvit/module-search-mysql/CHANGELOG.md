# Change Log
## 1.0.38
*(2020-04-13)*

#### Improvements
* Performance optimisations

---


## 1.0.37
*(2020-02-18)*

#### Fixed
* Magento 2.3.4 Compatibility (Tests)

---


## 1.0.36
*(2019-11-11)*

#### Fixed
* Ambiguous class declaration

---

## 1.0.35
*(2019-08-13)*

#### Fixed
* Marketplace compatibility

---

## 1.0.34
*(2019-08-06)*

#### Fixed
* Advanced search issue

---

## 1.0.33
*(2019-06-27)*

#### Fixed
* Stability

---

## 1.0.32
*(2019-06-27)*

#### Fixed
* Magento 2.3.2 compatibility

---
## 1.0.31
*(2019-05-08)*

#### Fixed
* eqp test fix

---

## 1.0.29
*(2018-02-25)*

#### Fixed
* DI compilation issue

---


## 1.0.28
*(2018-02-25)*

#### Fixed
* compatibility with Magento 2.1.14 EE

---


## 1.0.27
*(2018-12-10)*

#### Fixed
* M2.3 Index Switcher Error

---


## 1.0.26
*(2018-11-29)*

#### Fixed
* Compatibility with Magento 2.3

---


## 1.0.25
*(2018-10-16)*

#### Fixed
* "Instance for _  not found"

---


## 1.0.24
*(2018-10-12)*

#### Fixed
* Issue with undefined offset (built-in engine) 

---

## 1.0.23
*(2018-10-09)*

#### Improvements
* Performance

---

## 1.0.22
*(2018-09-26)*

#### Fixed
* Issue with offset 1

---


## 1.0.21
*(2018-09-21)*

#### Improvements
* Performance

---


## 1.0.20
*(2018-09-21)*

#### Fixed
* Processing multiselect attributes

---

## 1.0.19
*(2018-09-20)*

#### Fixed
* Issue during indexation

---


## 1.0.18
*(2018-09-20)*

#### Fixed
* Issue with reindex

---


## 1.0.17
*(2018-09-19)*

#### Fixed
* Compatibility with Magento 2.1

---

## 1.0.16
*(2018-09-12)*

#### Features
* Added functionality to use multiple Catalog Attribute index

---


## 1.0.15
*(2018-09-07)*

#### Fixed
* M2.1

---


## 1.0.14
*(2018-09-05)*

#### Improvements
* Compatibility with Magento 2.1

---


## 1.0.13
*(2018-01-30)*

#### Fixed
* Issue with searching by custom options

---



### 1.0.12
*(2017-11-20)*

#### Fixed
* Issue with search_weight column

---

### 1.0.11
*(2017-11-16)*

#### Fixed
* Issue with weight

---

### 1.0.10
*(2017-11-15)*

#### Fixed
* Issue with column search_weight

---

### 1.0.9
*(2017-11-13)*

#### Fixed
* Issue with index switcher

---

### 1.0.8
*(2017-09-27)*

#### Fixed
* Switcher

---

### 1.0.5
*(2017-09-26)*

#### Fixed
* Indexer switcher

---

### 1.0.4
*(2017-08-08)*

#### Fixed
* Issue with 'Not Words'
* Issue with stopwords

---

### 1.0.3
*(2017-05-04)*

#### Fixed
* Issue with empty query after applying stopwords

---

### 1.0.2
*(2017-04-13)*

#### Fixed
* Match logic

---

### 1.0.1
*(2017-04-10)*

#### Improvements
* Added suggestion provider for AdvancedSearch

------
