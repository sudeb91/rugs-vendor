# Change Log
##  Search Spell Correction [mirasvit/module-misspell]


## 1.0.34
*(2019-10-08)*

#### Fixed
* Misspell split functionality
* Set misspell tables to MyISAM engine


---


## 1.0.33
*(2019-09-18)*

#### Fixed
* Spell correction don't return suggested result

---


## 1.0.32
*(2019-08-13)*

#### Fixed
* Marketplace compatibility

---


## 1.0.31
*(2019-05-27)*

#### Fixed
* Generators cannot return values using “return” 

---


## 1.0.29
*(2019-02-12)*

#### Fixed
* Allowed memory size error

---


## 1.0.28
*(2018-11-29)*

#### Fixed
* Compatibility with Magento 2.3

---


## 1.0.27
*(2018-10-01)*

#### Fixed
* ECHO

---



## 1.0.26
*(2018-09-19)*

#### Fixed
* Issue with first suggesting in some cases

---


## 1.0.24
*(2018-05-31)*

#### Fixed
* Issue with indexation cyrilic terms

---


## 1.0.23
*(2018-04-11)*

#### Fixed
* Issue with error 22003

---



## 1.0.22
*(2017-12-25)*

#### Improvements
* Integrated with Search Autocomplete
* Added Reindex validator

---

### 1.0.21
*(2017-12-13)*

#### Improvements
* Fallback search logic

---

### 1.0.20
*(2017-11-17)*

#### Fixed
* Issue with _cl table

---

### 1.0.19
*(2017-10-26)*

#### Fixed
* Possible issue with null values during indexation

---

### 1.0.18
*(2017-09-28)*

#### Fixed
* Issue with calculation number of results for suggested search phrase

---

### 1.0.17
*(2017-09-26)*

#### Fixed
* M2.2
* Issue with highlighting

---

### 1.0.16
*(2017-08-09)*

#### Fixed
* Issue with check zero result

---

### 1.0.15
*(2017-07-12)*

#### Fixed
* Issue with Changelog changes

---

### 1.0.14
*(2017-07-10)*

#### Improvements
* Fallback search logic

---

### 1.0.13
*(2017-06-20)*

#### Fixed
* Compatibility issue with Amasty Shopby

---

### 1.0.12
*(2017-05-10)*

#### Improvements
* Remove spell correction index if it disabled

---

### 1.0.11
*(2017-04-11)*

#### Improvements
* Switched to API interfaces

---

### 1.0.10
*(2017-02-20)*

#### Improvements
* Changed all string fuctions to mb_*

---


### 1.0.9
*(2017-02-03)*

#### Improvements
* Added Recurring setup script for check fulltext indices

---

### 1.0.8
*(2016-11-21)*

#### Improvements
* Compatibility with M 2.2.0

---

### 1.0.7
*(2016-06-24)*

#### Fixed
* Compatibility with Magento 2.1

---

### 1.0.6
*(2016-06-16)*

#### Fixed
* Fixed an issue with changing index mode for misspell index

---

### 1.0.5
*(2016-04-27)*

#### Improvements
* Improved extension performance
* i18n

#### Documentation
* Updated installation steps

---

### 1.0.4
*(2016-02-23)*

#### Fixed
* Fixed an issue with segmentation fault during reindex (PHP7)

---

### 1.0.3
*(2016-02-07)*

#### Documentation
* Added user manual


##  Search Autocomplete & Suggest [mirasvit/module-search-autocomplete]


## 1.1.105
*(2019-01-02)*

#### Fixed
* Disable native search autocomplete

---

## 1.1.104
*(2019-12-18)*

#### Fixed
* Blackbird contentmanager index

---

## 1.1.103
*(2019-12-16)*

#### Fixed
* Add blackbird contentmanager index

#### Improvements
* Product search index refactoring

---

## 1.1.102
*(2019-12-09)*

#### Fixed
* Rating issue

---

## 1.1.101
*(2019-12-03)*

#### Fixed
* Wrong search results breadcrumbs
* Rating issue

---


## 1.1.100
*(2019-11-25)*

#### Improvements
* Use default magento price block for search autocomplete

---


## 1.1.99
*(2019-11-25)*

#### Fixed
* Unable to apply 'Add to Cart' translation
* Missing product rating
* Category index results wrong urls in fast mode
* CMS page index results wrong urls in fast mode

---


## 1.1.98
*(2019-11-14)*

#### Fixed
* Conflict with Webkul ShowPriceAfterlogin

---


## 1.1.97
*(2019-11-12)*

#### Fixed
* Search Button is not clickable when selecting the term from the Hot Searches

---


## 1.1.96
*(2019-08-08)*

#### Fixed
* Issue with wrong layer

---


## 1.1.95
*(2019-08-06)*

#### Fixed
* Prices issue for multistore setup in 'Fast Mode'
* Product thumbnails issue in 'Fast Mode'

---


## 1.1.94
*(2019-07-31)*

#### Fixed
* Issue with autocomplete visibility, even if cart popoup was showed

---


## 1.1.93
*(2019-07-30)*

#### Features
* Fishpig Glossary index support

#### Fixed
* native search form active state
* nested environment emulation error
* reindex speedup
* Blinking autocomplete box with multiple search forms on the same page

---


## 1.1.92
*(2019-06-19)*

#### Fixed
* Render html entities on server side
* KB article typo in template
* Remove .active when on autocomplete miss focus


## 1.1.91
*(2019-04-26)*

#### Fixed
* conflict with IE 10

#### Improvements
* Added message after fast mode enable


## 1.1.90
*(2019-04-24)*

#### Fixed
* Ensure search autocomplete Fast Mode config file on reindex
* Display Fast mode indexes in correct order
---


## 1.1.89
*(2019-04-12)*

#### Fixed
* incorrect module conflict declaration
---


## 1.1.88
*(2019-04-08)*

#### Fixed
* Similar results in multiple attribute indexes

---


## 1.1.87
*(2019-04-01)*

#### Fixed
* Translations for search in stores with fast mode

#### Improvements
* Improved weighting, ability to use advansed search options, synonyms, stopwords
---


## 1.1.86
*(2019-03-13)*

#### Fixed
* Search in stores with fast mode

---




##  Search Elastic [mirasvit/module-search-elastic]


## 1.2.58
*(2019-12-30)*

#### Fixed
* Issue with store param in the autocomplete fast mode

---


## 1.2.57
*(2019-12-10)*

#### Fixed
* Elasticsearch 7 compatibility

---

## 1.2.56
*(2019-11-21)*

#### Fixed
* Compatibility Stock Filter with Amasty Shopby

---

## 1.2.54
*(2019-11-11)*

#### Fixed
* Ambiguous class declaration

---


## 1.2.53
*(2019-11-08)*

#### Fixed
* Undefined index :"price"

---


## 1.2.52
*(2019-10-24)*

#### Improvements
* Performance optimization

#### Fixed
* Issue with special price

---


## 1.2.51
*(2019-10-16)*

#### Improvements
* Removed "nested" filters functionality

---


## 1.2.50
*(2019-10-03)*

#### Fixed
* Use special price in price bucket

---


## 1.2.49
*(2019-08-29)*

#### Fixed
* Term filter issues

---


## 1.2.48
*(2019-08-28)*

#### Fixed
* EQP

---


## 1.2.47
*(2019-08-20)*

#### Fixed
* Price filter when Elasticsearch engine enabled

---


## 1.2.46
*(2019-08-13)*

#### Fixed
* Reindex issue when MSI disabled

---


## 1.2.45
*(2019-07-31)*

#### Fixed
* Advanced search issue
* Indexing with multi source inventory

---


## 1.2.44
*(2019-07-04)*

#### Fixed
* Reindex speed issue 
---

#### Improvements
* Ability to reset search indexes for current store


## 1.2.43
*(2019-06-27)*

#### Fixed
* Magento 2.3.2 compatibility
---


## 1.2.42
*(2019-05-22)*

#### Fixed
* missing filters when layered navigation multiselect enabled
---


## 1.2.41
*(2019-05-15)*

#### Fixed
* Magento EE indexation issue
* Search autocomplete fast mode
---


## 1.2.40
*(2019-04-24)*

#### Fixed
* Search Autocomplete Fast mode missing indexes
---


## 1.2.39
*(2019-04-08)*

#### Fixed
* Aggregations based on parent attributes
* Aggregations Dynamic bucket
* Catalog search and catalog categories downtime on search reindex
* Issue with synonyms in Search Autocomplete Fast mode
---


## 1.2.38
*(2019-04-01)*

#### Fixed
* Indexing issues

#### Improvements
* Ability to use advansed search options, synonyms, stopwords in fast mode

---


## 1.2.37
*(2019-03-28)*

#### Fixed
* support of magento 2.3.1

---


## 1.2.36
*(2019-03-21)*

#### Fixed
* search and filter using child products attributes

---


## 1.2.35
*(2019-03-13)*

#### Fixed
* Search in stores with fast mode

---




##  Search Landing Page [mirasvit/module-search-landing]


## 1.0.8
*(2019-09-09)*

#### Fixed
* EQP

---


## 1.0.7
*(2019-06-04)*

#### Fixed
* Issue with different url keys for landing pages on different stores

---


## 1.0.6
*(2018-11-29)*

#### Fixed
* Compatibility with Magento 2.3

---


## 1.0.5
*(2018-10-10)*

#### Improvements
* Multistore

---


## 1.0.4
*(2018-04-12)*

#### Features
* Allow redirect by search term to url key 

---

### 1.0.3
*(2017-09-26)*

#### Fixed
* M2.2

---

### 1.0.2
*(2017-07-25)*

#### Fixed
* Issue with static tests

---

### 1.0.1
*(2017-05-03)*

#### Fixed
* Issue with UI

---

### 1.0.0
*(2017-05-03)*

* Initial release

------

##  Search Report [mirasvit/module-search-report]


## 1.0.6
*(2019-11-14)*

#### Fixed
* Conflict with Paysera payment methods

---

## 1.0.5
*(2018-08-21)*

#### Fixed
* Report settings do not work

---

## 1.0.4
*(2018-04-20)*

#### Fixed
* Issue with report by search terms

---

## 1.0.3
*(2018-02-14)*

#### Improvements
* Switched to new module-report version

#### Fixed
* Added details for secure cookies
added details for secure cookies

---

### 1.0.2
*(2017-09-26)*

#### Fixed
* M2.2

---

### 1.0.1
*(2017-07-21)*

#### Fixed
* Possible issue with "Circular dependency"

---


