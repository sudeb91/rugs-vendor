# Change Log

### 1.0.8
*(2020-03-16)* 

* Code improvements

## 1.0.6
*(2019-11-14)*

#### Fixed
* Conflict with Paysera payment methods

---

## 1.0.5
*(2018-08-21)*

#### Fixed
* Report settings do not work

---

## 1.0.4
*(2018-04-20)*

#### Fixed
* Issue with report by search terms

---

## 1.0.3
*(2018-02-14)*

#### Improvements
* Switched to new module-report version

#### Fixed
* Added details for secure cookies
added details for secure cookies

---

### 1.0.2
*(2017-09-26)*

#### Fixed
* M2.2

---

### 1.0.1
*(2017-07-21)*

#### Fixed
* Possible issue with "Circular dependency"

---
