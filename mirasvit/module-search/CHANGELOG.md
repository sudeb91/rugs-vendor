# Change Log
## 1.0.149
*(2020-06-16)*
#### Improvements
* Added comment for incompatibility with Fast Mode Autocomplete options

#### Fixed
* Unexpected special char appears on highlight
* Mana_layerednavigationajax paging compatibility
* Incorrect stemming behavior

---


## 1.0.148
*(2020-06-02)*

#### Fixed
* Array to string conversion on reindex
* Amasty Blog posts links
* Unexpected numeric results
* Emulation nesting error
* Manage search results tabs display
* Category search returns relevance 0

---


## 1.0.147
*(2020-04-30)*

#### Fixed
* Category reindex issue on Magento 2.3.5 Enterprise
* select attributes override

---


### 1.0.146
*(2020-04-23)* 

#### Fixed

* Score boost rules indexing issue
* Filter out suggested search terms with mysql entries
* CMS pages indexing issue
* Weltpixel LRN compatibility


## 1.0.145
*(2020-04-14)*

#### Fixed
* weltpixel LRN compatibility 
* CMS pages indexing issue
* filter out suggested search terms with mysql entries
* score boost rules indexing issue

---


## 1.0.144
*(2020-04-07)*

#### Fixed
* disallow json encoded values for elasticsearch indexer
* highlight search result text case override
* multi-select attributes reindex issue when Search by child products disabled ([#250]())
* highlight search result text case override
* ignore empty categories on reindex

---

## 1.0.143
*(2020-03-18)*

#### Fixed
* Bundle products indexing issue (include child products by default)
* Decrease score rules indexing time
* Magento SharedCatalog search issue
* Popular suggestions don't support wildcard exceptions
* Upgrade schema issue

---

## 1.0.142
*(2020-03-05)*

#### Fixed
* Issue with serialization on magento 2.1

---

## 1.0.141
*(2020-02-18)*

#### Fixed
* Issue with plugin loadEntities

---


## 1.0.140
*(2020-02-13)*

#### Fixed
* disable query log (for some reasons SQL queries are shown on frontend)

---


## 1.0.139
*(2020-02-12)*

#### Fixed
* Environment emulation nesting is not allowed error when CMS page index enabled
* Duplicate entry mst_search_weight on Setup upgrade
* Search highlight issue (replacement is applied to placeholder)

---


## 1.0.138
*(2020-02-03)*

#### Fixed
* Magento 2.3.4 compatibility

---


## 1.0.137
*(2019-12-17)*

#### Fixed
* Wrong highlight behavior
* Add deprecated classes

---


## 1.0.136
*(2019-11-28)*

#### Fixed
* Highlight issue

---


## 1.0.135
*(2019-11-27)*

#### Fixed
* Issue with unicode

---


## 1.0.134
*(2019-11-25)*

#### Fixed
* Show empty results if search query is empty or minimum query length

---


## 1.0.133
*(2019-11-13)*

#### Fixed
* Multi-store results function doens't redirect properly

---


## 1.0.132
*(2019-11-11)*

#### Fixed
* Ambigious class declaration
* Undefined elastic factories
* Error while setting up M2.1.12 EE

---


## 1.0.131
*(2019-10-16)*

#### Fixed
* Compatibility with Magento 2.3.3
* Score rule apply issue

---


## 1.0.130
*(2019-10-09)*

#### Fixed
* Individual search weight saving issue

---


## 1.0.129
*(2019-10-09)*

#### Fixed
* Display informative errors on synonyms and stopwords import
* Duplicate duplicate cms page index ignore page options
* Individual search weight saving issue

---


## 1.0.128
*(2019-09-10)*

#### Fixed
* EQP (each)
* Issue with validator form (M 2.3.0)

---


## 1.0.127
*(2019-09-03)*

#### Fixed
* Issue with filtration of Synonyms/Stopwords/Score Rules (backend)

---


## 1.0.126
*(2019-08-28)*

#### Improvements
* Synonyms import

---


## 1.0.125
*(2019-08-27)*

#### Fixed
* Issue with YAML

---


## 1.0.124
*(2019-07-30)*

#### Fixed
* Issue with validator
* ICanSearchProductId Test
* advanced search issue
* Issue with Magento 2.3.2 after switch to MySQL engine

---


## 1.0.123
*(2019-07-08)*

#### Fixed
* Magento 2.3.2 advanced search issue

#### Features
* Fishpig Glossary index support

---


## 1.0.122
*(2019-06-13)*

#### Fixed
* Mass update search_weight
* Issue with compilation (with TemplateMonster/AjaxCatalog)
* Moved messages about possible magento modules which contain "search in name" 
* Compatibility with Magento 2.3.1 Elasticsearch
* Doubled field values in CMS index.

---


## 1.0.121
*(2019-04-24)*

#### Fixed
* Fast mode ensure issue
* Mageplaza fix hint

## 1.0.120
*(2019-04-08)*

#### Fixed
* Similar results in multiple attribute indexes
* Environment emulation nesting is not allowed 

#### Features
* Ensure Search Autocomplete fast mode on search reindex

---

## 1.0.119
*(2019-03-21)*

#### Fixed
* Manadev compatibility fix
---

## 1.0.118
*(2019-03-19)*

#### Fixed
* Manadev compatibility
---

## 1.0.117
*(2019-01-04)*

#### Fixed
* JS issue with index attributes
* Solved conflict with Mageplaza_LayeredNavigation
---

## 1.0.116
*(2018-12-29)*

#### Improvements
* Added ability to search by AW Blog Post tags
* Mageplaza ajax layer

#### Fixed
* compatibility with TemplateMonsters_AjaxCatalog

---


## 1.0.114
*(2018-12-25)*

#### Improvements
* Rename column search_weight to mst_search_weight for prevent possible conflicts after disabling the module
* Compatibility with BlueFoot

---


## 1.0.113
*(2018-12-14)*

#### Fixed
* Issue with saving index attributes (for new indexes) 

---


## 1.0.112
*(2018-12-13)*

#### Features
* Catalog image is clickable

#### Fixed
* Issue with store switcher url

---

## 1.0.111
*(2018-12-06)*

#### Fixed
* Issue with store switcher on multistore search results [#87]

---


## 1.0.110
*(2018-12-06)*

#### Fixed
* switch stores on multistore results [#87]

---


## 1.0.109
*(2018-12-05)*

#### Fixed
* Issue with Search Weight during mass product update

---


## 1.0.108
*(2018-11-29)*

#### Fixed
* Compatibility with Magento 2.3
* wrong results for queries with specific characters

---


## 1.0.107
*(2018-11-15)*

#### Fixed
* Allow to cache search results #82
* Search by child products issue, bundles included even with disabled function #186

---


## 1.0.106
*(2018-11-13)*

#### Fixed
* Push out of stock products to the end issue #179

---


## 1.0.105
*(2018-11-13)*

#### Improvements
* Migration validation for WeltPixel_CmsBlockScheduler

#### Fixed
* Issue with class generation on Magento Cloud
* Highlight issue with special chars 

---


## 1.0.104
*(2018-11-05)*

#### Fixed
* Styling issue with Aheadworks blog

---


## 1.0.103
*(2018-11-05)*

#### Fixed
* Issue with highlights

---


## 1.0.102
*(2018-11-02)*

#### Fixed
* PHP 5.6 Syntax Error

---


## 1.0.101
*(2018-10-29)*

#### Features
* Added validator to detect different search engine settings

#### Fixed
* Issue with products index edit

---


## 1.0.100
*(2018-10-15)*

#### Fixed
* Issue with slow admin load (JS render time)

---


## 1.0.99
*(2018-10-12)*

#### Fixed
* Bundled products indexing issue
* Highlighter issue

---

## 1.0.98
*(2018-10-09)*

#### Fixed
* Reindex issue using mirasvit:search:reindex

---

## 1.0.97
*(2018-10-09)*

#### Fixed
* Issue with autocomplete provider
* Highlighter issue

---


## 1.0.96
*(2018-10-03)*

#### Fixed
* Issue with attribute

---


## 1.0.95
*(2018-10-03)*

#### Fixed
* Ves Blog indexing issue

---


## 1.0.94
*(2018-10-01)*

#### Features
* Add other search results to product results if results QTY less then 5

---


## 1.0.93
*(2018-09-28)*

#### Features
* Show Category Thumbnail in the search results 

---


## 1.0.92
*(2018-09-26)*

#### Fixed
* Issue with ContentManager

---


## 1.0.91
*(2018-09-26)*

#### Features
* Blackbird ContentManager Search Index

#### Fixed
* Issue with required core version

---


## 1.0.90
*(2018-09-21)*

#### Fixed
* Processing multiselect attributes

---


## 1.0.89
*(2018-09-21)*

#### Fixed
* Issue with module disable plugin

---


## 1.0.88
*(2018-09-21)*

#### Improvements
* Validator (Check possible conflicts with other search extensions)

---


## 1.0.87
*(2018-09-20)*

#### Improvements
* Added Amasty Blog Search Index

---


## 1.0.86
*(2018-09-20)*

#### Fixed
* Reindex issue with native mysql engine
* Fixed issue after module disable 

---


## 1.0.85
*(2018-09-18)*

#### Fixed
* Issue with unavailable index type on index edit screen

---


## 1.0.84
*(2018-09-17)*

#### Improvements
* Support multiple indexes for magento_catalog_attribute
* Added functionality to use multiple Catalog Attribute index

---


## 1.0.83
*(2018-09-11)*

#### Fixed
* Bug with ScoreServiceInterface

---


## 1.0.82
*(2018-09-10)*

#### Improvements
* Added addititonal functionality to Score Rules

#### Fixed
* Score Rule Save & Continue is not working for new rules

---


## 1.0.81
*(2018-09-06)*

#### Fixed
* ACL

---


## 1.0.80
*(2018-09-06)*

#### Improvements
* Added Score Boost Rule
* Added Apply button to edit form
* Support 2.1

#### Fixed
* UI component load error (2.1.2)
* Issue with SKU weight

---


## 1.0.79
*(2018-08-27)*

#### Improvements
* Custom weight apply logic

#### Fixed
* Issue with attributes synchronization

---


## 1.0.78
*(2018-08-01)*

#### Features
* Search Index for Amasty FAQ

#### Fixed
* fixed SSL certificate verify failed issue in search autocomplete speed validator ([#47](../../issues/47))

---


## 1.0.77
*(2018-06-08)*

#### Fixed
* Issue with empty node

---


## 1.0.76
*(2018-05-17)*

#### Fixed
* search only by active categories option
* wrong Sold QTY attribute settings

---


## 1.0.75
*(2018-03-06)*

#### Features
* Added search results validator and search speed test
* Added functionality to adjust relevance based on sold items quantity

---


## 1.0.74
*(2018-03-06)*

#### Fixed
* Issue with ordering

---



## 1.0.73
*(2018-03-06)*

#### Features
* Create search index for Mirasvit Gift Registry extension #25

---


## 1.0.72
*(2018-02-13)*

#### Improvements
* New search index: AW Blog

---


## 1.0.71
*(2018-02-12)*

#### Fixed
* Translation

---


## 1.0.70
*(2018-02-01)*

#### Fixed
* Issue with special chars (%) in suggested queries

---


## 1.0.69
*(2018-01-30)*

#### Fixed
* Issue with multi-store results

---


## 1.0.68
*(2018-01-16)*

#### Fixed
* Translations in suggestion.phtml

---


## 1.0.67
*(2018-01-15)*

#### Improvements
* Engine status visualization

#### Fixed
* Mageplaza blog index

---


## 1.0.66
*(2018-01-09)*

#### Fixed
* Issue with autocomplete

---



### 1.0.65
*(2017-12-14)*

#### Fixed
* Magento 2.2.2 - removed symfony/yaml requirement

---

### 1.0.64
*(2017-12-14)*

#### Improvements
* Strip tags method for Cms Pages index

---

### 1.0.63
*(2017-12-13)*

#### Improvements
* Changes related to search in categories functionality (#6)

---

### 1.0.62
*(2017-12-06)*

#### Fixed
* Performance issues with complex synonyms

---

### 1.0.61
*(2017-12-01)*

#### Improvements
* Ability to run search reindex for specified store or index (bin/magento mirasvit:search:reindex --store _id_ --index _identifier_)
* Code Formatting

---

### 1.0.60
*(2017-12-01)*

#### Fixed
* Issue with sorting products

---

### 1.0.59
*(2017-11-29)*

#### Fixed
* Added store filter to Magefan blog

---

### 1.0.58
*(2017-11-21)*

#### Improvements
* Recurring script for convert serialized values to JSON

---

### 1.0.57
*(2017-11-20)*

#### Fixed
* Issue with joining attributes

---

### 1.0.56
*(2017-11-17)*

#### Fixed
* Issue with long-tail expression form

---

### 1.0.55
*(2017-10-17)*

#### Improve
* Show/hide suggested search terms on search result page

---

### 1.0.54
*(2017-10-17)*

#### Fixed
* Issue with data-mappers
* Issue with Json decode

---

### 1.0.53
*(2017-10-12)*

#### Improvements
* Russian stemmer

#### Fixed
* Do not lowercase indexed text

---

### 1.0.52
*(2017-10-05)*

#### Improvements
* Added ability to select Match Mode (AND or OR)

---

### 1.0.51
*(2017-09-28)*

#### Fixed
* Issue with unserialize (replaced with JSON)

---

### 1.0.50
*(2017-09-27)*

#### Fixed
* M2.2
* Issue with No Results page
* UI error on index edit page

---

### 1.0.47
*(2017-09-08)*

#### Fixed
* Issue with product mapper

---

### 1.0.46
*(2017-09-06)*

#### Fixed
* Issue with Search Report
* Strip tags filter

---

### 1.0.45
*(2017-09-05)*

#### Fixed
* Improved stripTags method

---

### 1.0.44
*(2017-09-04)*

#### Improvements
* Links to manual

#### Fixed
* Weights synchronization

---

### 1.0.43
*(2017-08-31)*

#### Fixed
* No results in search reports

---

### 1.0.42
*(2017-08-14)*

#### Fixed
* Issue with tab
* properly emulate store environment

---

### 1.0.41
*(2017-08-08)*

#### Fixed
* Issue with slow js rendering (backend)

---
### 1.0.40
*(2017-08-04)*

#### Fixed
* Ability to sort products by stock status

---
### 1.0.39
*(2017-07-28)*

#### Fixed
* Synonyms

---

### 1.0.37
*(2017-07-21)*

#### Fixed
* Responsive styles for indexes
* Convert synonyms/stopwords to lowercase before save
* Issue with blog indexation

---
### 1.0.36
*(2017-06-30)*

#### Fixed
* Issue with local Synonyms/Stopword dicitonary

---
### 1.0.35
*(2017-06-27)*

#### Improvements
* Added additional params to build urls for wordpress blog

#### Fixed
* Issue with weights

---
### 1.0.34
*(2017-06-22)*

#### Fixed
* Issue with index invalidation

---

### 1.0.33
*(2017-06-21)*

#### Improvements
* Added option to force sort order for products

---

### 1.0.32
*(2017-06-19)*

#### Fixed
* Bundled Products (EE)
* Issue with synonyms

---

### 1.0.31
*(2017-06-19)*

#### Fixed
* Issue with mass delete

---

### 1.0.30
*(2017-06-16)*

#### Fixed
* Attribute
* Issue with attribute synchronization
* Issue with updating index status after change properties/attributes

---

### 1.0.27
*(2017-06-07)*

#### Improvements
* Media types for 404 to search

#### Fixed
* Installation script

---

### 1.0.26
*(2017-06-07)*

#### Improvements
* Backend UI

#### Fixed
* EE bundled

---

### 1.0.25
*(2017-05-29)*

#### Fixed
* CLI

---

### 1.0.24
*(2017-05-24)*

#### Fixed
* Issue with Replace Words

---

### 1.0.23
*(2017-05-24)*

#### Fixed
* Changed "Indices" to "Indexes"

---

### 1.0.22
*(2017-05-23)*

#### Fixed
* Issue with local synonyms/stopwords files

---

### 1.0.21
*(2017-05-18)*

#### Improvements
* Long tail hint

#### Fixed
* Issue with search_weight attribute
* Fixed an issue with custom search weight

---

### 1.0.20
*(2017-05-04)*

#### Improvements
* Reindex visualization

#### Fixed
* Issue with engine status checker

---

### 1.0.19
*(2017-04-26)*

#### Improvements
* New search index for Mageplaza blog

#### Fixed
* Issue with properties saving

---

### 1.0.18
*(2017-04-18)*

#### Fixed
* Fixed an issue with cms page reindex

---

### 1.0.17
*(2017-04-18)*

#### Fixed
* Fixed an issue with custom weights

---

### 1.0.16
*(2017-04-13)*

#### Fixed
* Fixed an issue with EngineResolver path

---

### 1.0.15
*(2017-04-12)*

#### Fixed
* Fixed an issue with EngineResolver path

---

### 1.0.14
*(2017-04-10)*

#### Fixed
* Issue with EE reindex
* Fixed an issue with autocomplete provider

---

### 1.0.13
*(2017-04-07)*

#### Fixed
* Fixed an error with index "Attribute"

---

### 1.0.12
*(2017-04-06)*

#### Fixed
* Issue with installation script

---

### 1.0.11
*(2017-04-06)*

#### Fixed
* Fixed an issue with saving index properties

---

### 1.0.10
*(2017-04-06)*

#### Improvements
* Added prefix for search indices tables

---

### 1.0.9
*(2017-04-05)*

#### Fixed
* Fixed an issue with clear installation

---

### 1.0.8
*(2017-04-05)*

#### Improvements
* Changed locale resolver interface for stemming

#### Fixed
* Fixed an issue with autocomplete provider

---

### 1.0.7
*(2017-04-04)*

#### Fixed
* Issue with autocomplete
* Fixed an issue with importing stopwords

---

### 1.0.6
*(2017-04-04)*

#### Fixed
* Minor fixes

---

### 1.0.5
*(2017-03-31)*

#### Fixed
* Issue with installation

---

### 1.0.4
*(2017-03-31)*

#### Fixed
* Fixed an issue with generators

---

### 1.0.3
*(2017-03-09)*

#### Fixed
* Fixed an issue with compilation
* Minor naming problem

---

### 1.0.2
*(2017-03-06)*

#### Improvements
* Improved synonyms import interface

#### Fixed
* Fixed an issue with synonyms

---

### 1.0.1
*(2017-03-03)*

#### Improvements
* Performance

#### Fixed
* Fixed an issue with indexation

---

### 1.0.0
*(2017-02-17)*

#### Improvements
* Cloud service for synonyms/stopwords
* Initial release after split mirasvit/module-search-sphinx

#### Fixed
* Fixed an issue with filter by out of stock products

------
