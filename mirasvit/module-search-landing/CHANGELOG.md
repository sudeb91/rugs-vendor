# Change Log

### 1.0.9
*(2020-03-05)* 

* Code improvements

## 1.0.8
*(2019-09-09)*

#### Fixed
* EQP

---


## 1.0.7
*(2019-06-04)*

#### Fixed
* Issue with different url keys for landing pages on different stores

---


## 1.0.6
*(2018-11-29)*

#### Fixed
* Compatibility with Magento 2.3

---


## 1.0.5
*(2018-10-10)*

#### Improvements
* Multistore

---


## 1.0.4
*(2018-04-12)*

#### Features
* Allow redirect by search term to url key 

---

### 1.0.3
*(2017-09-26)*

#### Fixed
* M2.2

---

### 1.0.2
*(2017-07-25)*

#### Fixed
* Issue with static tests

---

### 1.0.1
*(2017-05-03)*

#### Fixed
* Issue with UI

---

### 1.0.0
*(2017-05-03)*

* Initial release

------
