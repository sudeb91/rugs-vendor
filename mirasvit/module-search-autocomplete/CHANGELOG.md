# Change Log
## 1.2.2
*(2020-07-08)*

#### Fixed
* wrong price data in fast mode
* show search term in fast mode empty results text
* display search results in line
* open result in new tab by image shift click
* product reviews backward compatibility
* misproportioned images
* added comment for incompatibility with Fast Mode Autocomplete
* getRating fix
* decrease fast mode reindex server load
* unknown column "score" issue

---


## 1.2.1
*(2020-05-14)*

#### Fixed
* Amasty Blog posts links
* remove get params from URLs in fast mode for some cases

---


## 1.2.0
*(2020-05-07)*

#### Fixed
* compatibility with maria db
* increase search autocomplete fast mode reindex speed
* remove search box JS from checkout page to avoid conflicts with 3rd party

---


## 1.1.109
*(2020-04-24)*

#### Fixed
* ">" mark display issue
* no word wrap on results title
* filter out suggested search terms with mysql entries

---


## 1.1.108
*(2020-04-07)*

#### Fixed
* decrease server load in the fast mode
* wrong category url
* wrong add to cart action in the fast mode
* fast mode product urls different from regular results
* 'category product' index display issue
* apply fast mode translations

---


## 1.1.107
*(2020-03-04)*

#### Fixed
* old search results are visible while new search is running
* wrong product url in fast mode
* in-stock product filter already applied issue

---


## 1.1.106
*(2020-02-12)*

#### Fixed
* Invalid attribute name: store_id on reindex
* Category product arrow styling issue
* Wrong product url on multi-store results when fast mode enabled

---

## 1.1.105
*(2020-01-02)*

#### Fixed
* Disable native search autocomplete

---

## 1.1.104
*(2019-12-18)*

#### Fixed
* Blackbird contentmanager index

---

## 1.1.103
*(2019-12-16)*

#### Fixed
* Add blackbird contentmanager index

#### Improvements
* Product search index refactoring

---

## 1.1.102
*(2019-12-09)*

#### Fixed
* Rating issue

---

## 1.1.101
*(2019-12-03)*

#### Fixed
* Wrong search results breadcrumbs
* Rating issue

---


## 1.1.100
*(2019-11-25)*

#### Improvements
* Use default magento price block for search autocomplete

---


## 1.1.99
*(2019-11-25)*

#### Fixed
* Unable to apply 'Add to Cart' translation
* Missing product rating
* Category index results wrong urls in fast mode
* CMS page index results wrong urls in fast mode

---


## 1.1.98
*(2019-11-14)*

#### Fixed
* Conflict with Webkul ShowPriceAfterlogin

---


## 1.1.97
*(2019-11-12)*

#### Fixed
* Search Button is not clickable when selecting the term from the Hot Searches

---


## 1.1.96
*(2019-08-08)*

#### Fixed
* Issue with wrong layer

---


## 1.1.95
*(2019-08-06)*

#### Fixed
* Prices issue for multistore setup in 'Fast Mode'
* Product thumbnails issue in 'Fast Mode'

---


## 1.1.94
*(2019-07-31)*

#### Fixed
* Issue with autocomplete visibility, even if cart popoup was showed

---


## 1.1.93
*(2019-07-30)*

#### Features
* Fishpig Glossary index support

#### Fixed
* native search form active state
* nested environment emulation error
* reindex speedup
* Blinking autocomplete box with multiple search forms on the same page

---


## 1.1.92
*(2019-06-19)*

#### Fixed
* Render html entities on server side
* KB article typo in template
* Remove .active when on autocomplete miss focus


## 1.1.91
*(2019-04-26)*

#### Fixed
* conflict with IE 10

#### Improvements
* Added message after fast mode enable


## 1.1.90
*(2019-04-24)*

#### Fixed
* Ensure search autocomplete Fast Mode config file on reindex
* Display Fast mode indexes in correct order
---


## 1.1.89
*(2019-04-12)*

#### Fixed
* incorrect module conflict declaration
---


## 1.1.88
*(2019-04-08)*

#### Fixed
* Similar results in multiple attribute indexes

---


## 1.1.87
*(2019-04-01)*

#### Fixed
* Translations for search in stores with fast mode

#### Improvements
* Improved weighting, ability to use advansed search options, synonyms, stopwords
---


## 1.1.86
*(2019-03-13)*

#### Fixed
* Search in stores with fast mode

---


