<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-search-elastic
 * @version   1.2.68
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\SearchElastic\Adapter\Query;

use Magento\Framework\Search\Request\Query\Match;
use Magento\Framework\Search\Request\QueryInterface;
use Mirasvit\Search\Api\Service\QueryServiceInterface;
use Mirasvit\Search\Model\Config;

class MatchQuery
{
    private $queryService;

    public function __construct(
        QueryServiceInterface $queryService
    ) {
        $this->queryService = $queryService;
    }

    /**
     * @param array          $query
     * @param QueryInterface $matchQuery
     *
     * @return array
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function build(array $query, QueryInterface $matchQuery)
    {
        /** @var Match $matchQuery */

        $searchQuery = $this->queryService->build($matchQuery->getValue());

        $fields = ['options' => 1];
        foreach ($matchQuery->getMatches() as $match) {
            $field = $match['field'];
            if ($field == '*') {
                continue;
            }

            $boost          = isset($match['boost']) ? intval((string)$match['boost']) : 1; //sometimes boots is a Phrase
            $fields[$field] = $boost;
        }

        $query['bool']['must'][]['query_string'] = [
            'fields' => array_keys($fields),
            'query'  => $this->compileQuery($searchQuery),
        ];

        $searchTerms = array_filter(preg_split('#\s|-#siu', $matchQuery->getValue(), null, PREG_SPLIT_NO_EMPTY));
        $booster = 1;
        $useBooster = false;
        foreach ($searchTerms as $key => $term) {
            $searchTerms[$key] = strtolower($term);
            if (strlen($term) == 1) {
                $useBooster = true;
            }
        }

        foreach ($fields as $field => $boost) {
            if ($boost > 1) {
                foreach ($searchTerms as $term) {
                    if ($useBooster) {
                        $booster = strlen($term) / strlen($matchQuery->getValue());
                    }

                    if (strlen($term) == 1) {
                        $query['bool']['should'][]['wildcard'][$field] = [
                            'value' => $term,
                            'boost' => pow(2, round($boost * $booster/2)),
                        ];
                    } else {
                        $query['bool']['should'][]['wildcard'][$field] = [
                            'value' => $term,
                            'boost' => pow(2, round($boost * $booster)),
                        ];

                        $term = '*' . $term . '*';

                        $query['bool']['should'][]['wildcard'][$field] = [
                            'value' => $term,
                            'boost' => pow(2, round($boost * $booster)-1),
                        ];
                    }
                }
            }
        }

        return $query;
    }

    /**
     * @param array $query
     *
     * @return string
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    private function compileQuery($query)
    {
        $compiled = [];
        foreach ($query as $directive => $value) {
            switch ($directive) {
                case '$like':
                    $compiled[] = '(' . $this->compileQuery($value) . ')';
                    break;

                case '$!like':
                    $compiled[] = '(NOT ' . $this->compileQuery($value) . ')';
                    break;

                case '$and':
                    $and = [];
                    foreach ($value as $item) {
                        $and[] = $this->compileQuery($item);
                    }
                    $compiled[] = '(' . implode(' AND ', $and) . ')';
                    break;

                case '$or':
                    $or = [];
                    foreach ($value as $item) {
                        $or[] = $this->compileQuery($item);
                    }
                    $compiled[] = '(' . implode(' OR ', $or) . ')';
                    break;

                case '$term':
                    $phrase = $this->escape($value['$phrase']);
                    switch ($value['$wildcard']) {
                        case Config::WILDCARD_INFIX:
                            $compiled[] = "$phrase OR *$phrase*";
                            break;
                        case Config::WILDCARD_PREFIX:
                            $compiled[] = "$phrase OR *$phrase";
                            break;
                        case Config::WILDCARD_SUFFIX:
                            $compiled[] = "$phrase OR $phrase*";
                            break;
                        case Config::WILDCARD_DISABLED:
                            $compiled[] = $phrase;
                            break;
                    }
                    break;
            }
        }

        return implode(' AND ', $compiled);
    }

    /**
     * @param string $value
     *
     * @return string
     */
    private function escape($value)
    {
        $pattern = '/(\+|-|\/|&&|\|\||!|\(|\)|\{|}|\[|]|\^|"|~|\*|\?|:|\\\)/';
        $replace = '\\\$1';

        return preg_replace($pattern, $replace, $value);
    }
}
