# Change Log
## 1.2.68
*(2020-06-23)*

#### Improvements
* Added comment for Fast Mode option

#### Fixed
* Fast mode save problem
* Multiselect attributes indexing issue
* Missing MSI default stock

---
## 1.2.67
*(2020-06-09)*

#### Improvements
* Added tooltip for fast mode option

#### Fixed
* Search autocomplete fast mode ES7 incompatibility

---
## 1.2.66
*(2020-05-25)*

#### Fixed
* Missing attribute options in layered navigation
* Products missing after error in bulk request
* MSI indexing issue
* ElasticSearch7 price filters issue


---
## 1.2.65
*(2020-04-30)*

#### Fixed
* Compatibility with Magento 2.3.5

---


## 1.2.64
*(2020-04-23)*

#### Fixed
* Magento EE: unknown column row_id while reindex

---


## 1.2.63
*(2020-04-14)*

#### Fixed
* bundle products price index issue
* add search term weight-to-length dependency
* missing add to cart button in fast mode
* exclude json values from indexing
* remove sphinx search entries in module acl
* ignore out of stock options
* ?debug=<any> => ?debug=search

---


## 1.2.62
*(2020-03-06)*

#### Fixed
* Missing search autocomplete results in fast mode

---


## 1.2.61
*(2020-03-04)*

#### Fixed
* Prevention random product position, if search results score (relevance) is the same for few products
* Spinner doesnt hide when nothing found in search autocomplete
* Bundle products indexing issue (include child products by default)

---


## 1.2.60
*(2020-02-19)*

#### Fixed
* Missing categories IDs after reindex, affects category page listing and filter by category

---


## 1.2.59
*(2020-02-11)*

#### Fixed
* Unable to save product from REST API

---


## 1.2.58
*(2019-12-30)*

#### Fixed
* Issue with store param in the autocomplete fast mode

---


## 1.2.57
*(2019-12-10)*

#### Fixed
* Elasticsearch 7 compatibility

---

## 1.2.56
*(2019-11-21)*

#### Fixed
* Compatibility Stock Filter with Amasty Shopby

---

## 1.2.54
*(2019-11-11)*

#### Fixed
* Ambiguous class declaration

---


## 1.2.53
*(2019-11-08)*

#### Fixed
* Undefined index :"price"

---


## 1.2.52
*(2019-10-24)*

#### Improvements
* Performance optimization

#### Fixed
* Issue with special price

---


## 1.2.51
*(2019-10-16)*

#### Improvements
* Removed "nested" filters functionality

---


## 1.2.50
*(2019-10-03)*

#### Fixed
* Use special price in price bucket

---


## 1.2.49
*(2019-08-29)*

#### Fixed
* Term filter issues

---


## 1.2.48
*(2019-08-28)*

#### Fixed
* EQP

---


## 1.2.47
*(2019-08-20)*

#### Fixed
* Price filter when Elasticsearch engine enabled

---


## 1.2.46
*(2019-08-13)*

#### Fixed
* Reindex issue when MSI disabled

---


## 1.2.45
*(2019-07-31)*

#### Fixed
* Advanced search issue
* Indexing with multi source inventory

---


## 1.2.44
*(2019-07-04)*

#### Fixed
* Reindex speed issue 
---

#### Improvements
* Ability to reset search indexes for current store


## 1.2.43
*(2019-06-27)*

#### Fixed
* Magento 2.3.2 compatibility
---


## 1.2.42
*(2019-05-22)*

#### Fixed
* missing filters when layered navigation multiselect enabled
---


## 1.2.41
*(2019-05-15)*

#### Fixed
* Magento EE indexation issue
* Search autocomplete fast mode
---


## 1.2.40
*(2019-04-24)*

#### Fixed
* Search Autocomplete Fast mode missing indexes
---


## 1.2.39
*(2019-04-08)*

#### Fixed
* Aggregations based on parent attributes
* Aggregations Dynamic bucket
* Catalog search and catalog categories downtime on search reindex
* Issue with synonyms in Search Autocomplete Fast mode
---


## 1.2.38
*(2019-04-01)*

#### Fixed
* Indexing issues

#### Improvements
* Ability to use advansed search options, synonyms, stopwords in fast mode

---


## 1.2.37
*(2019-03-28)*

#### Fixed
* support of magento 2.3.1

---


## 1.2.36
*(2019-03-21)*

#### Fixed
* search and filter using child products attributes

---


## 1.2.35
*(2019-03-13)*

#### Fixed
* Search in stores with fast mode

---


